<?php

namespace Drupal\shepherd\Cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Lock\LockBackendInterface;

class ShepherdBackend implements CacheBackendInterface {

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $inner;

  /**
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * @var array
   */
  protected $data = [];

  protected $timeout;

  public function __construct(CacheBackendInterface $inner, LockBackendInterface $lock, $timeout) {
    $this->inner = $inner;
    $this->lock = $lock;
    $this->timeout = $timeout;
  }

  /**
   * {@inheritDoc}
   */
  public function get($cid, $allow_invalid = FALSE) {
    $return = $this->inner->get($cid, $allow_invalid);
    if (!$return && !$this->lock->acquire('shepherd')) {
      $this->lock->wait('shepherd', $this->timeout);
      $return = $this->inner->get($cid, $allow_invalid);
    }
    return $return;
  }

  /**
   * {@inheritDoc}
   */
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = []) {
    $this->data = [$cid, $data, $expire, $tags];
  }

  public function __destruct() {
    if ($this->data) {
      $this->inner->set($this->data[0], $this->data[1], $this->data[2], $this->data[3]);
    }
    $this->lock->release('shepherd');
  }

  /**
   * {@inheritDoc}
   */
  public function deleteAll() {
    $this->inner->deleteAll();
  }

  /**
   * {@inheritDoc}
   */
  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function setMultiple(array $items) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function delete($cid) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function deleteMultiple(array $cids) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function invalidate($cid) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateMultiple(array $cids) {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateAll() {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function garbageCollection() {
    throw new \LogicException('This method is not supported');
  }

  /**
   * {@inheritDoc}
   */
  public function removeBin() {
    throw new \LogicException('This method is not supported');
  }

}
