This module protects your site from the "thundering herd" problem. After a
cache rebuild, one process gets the chance to finish loading the page, the
rest waits for it. This waiting time can be set in seconds by using
$settings['shepherd_timeout'], defaults to 2.

To activate the module, add

include_once $app_root . '/modules/custom/shepherd/settings.shepherd.php';

to settings.php. This must come after $settings['shepherd_timeout'] if that
is used. The module can not be enabled.

An example deployment script is provided.
