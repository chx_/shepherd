<?php

use Drupal\Core\Cache\DatabaseBackend;

// This is an extended copy of
// \Drupal\Core\DrupalKernel::$defaultBootstrapContainerDefinition.
if (PHP_SAPI !== 'cli') {
  include_once __DIR__ . '/src/Cache/ShepherdBackend.php';
  $settings['bootstrap_container_definition'] = [
    'parameters' => [],
    'services' => [
      'database' => [
        'class' => 'Drupal\Core\Database\Connection',
        'factory' => 'Drupal\Core\Database\Database::getConnection',
        'arguments' => ['default'],
      ],
      'lock' => [
        'class' => 'Drupal\Core\Lock\DatabaseLockBackend',
        'arguments' => ['@database'],
      ],
      'cache.container.inner' => [
        'class' => 'Drupal\Core\Cache\DatabaseBackend',
        'arguments' => ['@database', '@cache_tags_provider.container', 'container', DatabaseBackend::MAXIMUM_NONE],
      ],
      'cache.container' => [
        'class' => 'Drupal\shepherd\Cache\ShepherdBackend',
        'arguments' => ['@cache.container.inner', '@lock', $settings['shepherd_timeout'] ?? 2]
      ],
      'cache_tags_provider.container' => [
        'class' => 'Drupal\Core\Cache\DatabaseCacheTagsChecksum',
        'arguments' => ['@database'],
      ],
    ],
  ];
}

