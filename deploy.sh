#!/bin/bash
mysql=$(drush sql-connect)
$mysql -e "INSERT INTO semaphore (name,expire) VALUES ('shepherd', UNIX_TIMESTAMP() + 300)"
drush -y updb
drush -y cim sync
wget https://www.example.com
$mysql -e "DELETE FROM semaphore WHERE name = 'shepherd'"
